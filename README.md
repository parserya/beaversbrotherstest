# Тестовое задание BeaverBrothers #

### Выполнено с помощью ###

* AngularJS
* ES6 + Babel
* SCSS(SASS)
* Gulp + JSON Server
* Соглашение об именование БЭМ

### Как запустить? ###

* Клонировать репозиторий
* Выполнить npm install для установки всех зависимостей
* Запустить сборщик + сервер статики + сервер API командой "gulp"
* Открыть в браузере [http://localhost:3000/](http://localhost:3000/)

### Информация обо мне ###

* Котиков Артем
* Телефон: +7 (965) 518-86-70
* Skype: parserya
* Email: parserya@yandex.ru