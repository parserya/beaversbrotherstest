import gulp from 'gulp';
import sass from 'gulp-sass';
import gutil from 'gulp-util';
import babelify from 'babelify';
import browserify from 'browserify';

import jsonServer from 'gulp-json-srv';
const server = jsonServer.create({
    port: 3333,
    baseUrl: '/api',
});


import browserSync from 'browser-sync';
const reload = browserSync.reload;

import source from 'vinyl-source-stream';

// JSON-сервер
gulp.task("server", () => {
    return gulp.src("./source/data.json")
        .pipe(server.pipe());
});

// Шрифты копируются в public
gulp.task('fonts', () => {
    gulp.src(['./source/fonts/**/*'])
        .pipe(gulp.dest('./public/fonts/'));
});

// Картинки копируются в public
gulp.task('images', () => {
    gulp.src(['./source/images/**/*'])
        .pipe(gulp.dest('./public/images/'));
});

// Сборка всего JS в bundle
gulp.task('browserify', () => {
    return browserify({
            entries: 'source/js/App.js',
            debug: false
        })
        .transform(babelify.configure({
            presets : ["es2015"]
        }))
        .bundle()
        .on('error', function(err){
            gutil.log(gutil.colors.red.bold('[browserify error]'));
            gutil.log(err.message);
            this.emit('end');
        })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./public'));
});

// Запускаем browser-sync для автообнолвений
gulp.task('browser-sync', () => {
    browserSync.init(["public/*.css", "public/*.js"], {
        server: {
            baseDir: "./"
        }
    });
});

// Сборка sass
gulp.task('sass', () => {
    gulp.src('./source/styles/main.scss')
    .pipe(sass({
            includePaths: require('node-normalize-scss').includePaths
        })
        .on('error', sass.logError)
    )
    .pipe(gulp.dest('./public/'))
    .pipe(reload({stream:true}));
});

// Набор watchers
gulp.task('watch', () => {
    gulp.watch('./source/js/**/*.js', ['browserify']);
    gulp.watch('./source/styles/**/*.scss', ['sass']);
});

gulp.task('build', ['browserify', 'sass', 'browser-sync']);
gulp.task('default', ['build', 'server', 'fonts', 'images', 'watch']);
