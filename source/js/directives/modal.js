class Modal {
    constructor() {
        this.restrict = 'EA';
        this.replace = true;
        this.transclude = true;
        this.templateUrl = 'source/html/modal.html';
        this.scope = {
            isOpen: '=',
            size: '@'
        };
    }

    link($scope, elem, attrs) {
        $scope.title = attrs.simpleModal;

        $scope.close = this.close;
    }

    close() {
        this.isOpen = false;
    }

    static factory() {
        return new Modal();
    }
}

Modal.factory.$inject = [];

export default Modal;
