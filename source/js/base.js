/*
 * Базовый контроллер нужен для добавления методов контроллера в $scope. обязательно надо вызвать super($scope);
  */
class BaseController {

    constructor($scope) {
        const props = Object.getPrototypeOf(this); // методы класса хранятся в прототипе
        const propsNames = Object.getOwnPropertyNames(props); // получаем список полей
        for (const name in propsNames) {
            const propName = propsNames[name];
            const prop = props[propName];
            if (prop instanceof Function) { // проверяем что поле - функция
                $scope[propName] = prop.bind(this); // обязательно биндим указатель на текущий объект
            }
        }
    }
}

export default BaseController;
