'use strict';

import angular from 'angular';
import PlacesLoader from './places';
import Modal from './directives/modal';

class App {
    static init() {
        const App = angular.module('BBTestApp', []);

        // Запускаем loader для всех контроллеров и сервисов
        PlacesLoader.load(App);
        App.directive('simpleModal', Modal.factory);
        return App;
    }
}

// Точка входа в приложение
App.init();
