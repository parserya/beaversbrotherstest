/*
 * Сервис для получения мест с сервера
 */

// Настройки хоста, к которому будет идти запрос
// const HOST = 'http://192.168.1.239:3333';
const HOST = 'http://localhost:3333';

class PlacesService {
    constructor($http) {
        this.$http = $http;
        this.cities = [];
        this.placesCount = -1;
    }

    /*
     * Получаем все места
     */
    loadPlaces() {
        return this.$http.get(HOST + '/api/loadPlaces')
            .then(data => {
                // Из всех полученных данных формируем список городов
                let cities = {};
                data.data.forEach((item) => {
                    cities[item.city] = true;
                });
                this.cities = Object.keys(cities);

                // Запоминаем общее количество всех мест(без фильтрации)
                this.placesCount = data.data.length;
                return data.data;
            });
    }
    
    /*
     * Получаем все места по городу
     */
    loadPlaceByCity(city) {
        return this.$http.get(HOST + '/api/loadPlaces?city=' + city)
            .then(data => data.data);
    }

    /*
     * Добавляем новое место
     */
    addPlace(place) {
        place.id = this.placesCount + 1;
        return this.$http.post(HOST + '/api/loadPlaces', place)
            .then(data => data.data);
    }

    /*
     * Возвращает список городов
     */
    getCities() {
        return this.cities;
    }
}

PlacesService.$inject = ['$http'];

export default PlacesService;
