import PlacesController from './places.controller';
import PlaceAddController from './place-add.controller';
import PlacesService from './places.service';

// Загрузчик всех контроллеров и сервисов, отвечающих за места
class PlacesLoader {
    static load(app) {
        app
            .service('PlacesService', PlacesService)
            .controller('PlacesController', PlacesController)
            .controller('PlaceAddController', PlaceAddController);
            
    }
}

export default PlacesLoader;
