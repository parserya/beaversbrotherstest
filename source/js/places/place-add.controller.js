/*
 * Контроллер добавления нового места
 */

import BaseController from '../base';

class PlaceAddController extends BaseController{
    constructor($scope, PlacesService) {
        super($scope);
        this.placesService = PlacesService;

        $scope.newPlace = {};
        this.$scope = $scope;
    }

    /*
     * Обработчик нажатия кнопки "Добавить свое место"
     */
    addPlace() {
        this.placesService.addPlace(this.$scope.newPlace)
            .then(data => {
                this.$scope.newPlace = {};
                this.$scope.modal.isPlaceAddOpen = false;
                this.$scope.chooseCity();
            });
    }
}

PlaceAddController.$inject = ['$scope', 'PlacesService'];

export default PlaceAddController;
