/*
 * Контроллер для работы с местами
 */

import BaseController from '../base';

class PlacesController extends BaseController{
    constructor($scope, PlacesService) {
        super($scope);
        this.placesService = PlacesService;

        // Различные данные для модальных окон
        $scope.modal = {
            isCitiesOpen: false,
            isPlaceAddOpen: false,
            cities: []
        };

        // Получаем список всех мест
        $scope.places = [];
        this.placesService.loadPlaces()
            .then(places => {
                $scope.places = places;
            });

        this.$scope = $scope;
    }

    /*
     * Получаем все города и показываем модальное окно с их выбором
     */
    getCities() {
        this.$scope.modal.cities = this.placesService.getCities();
        this.$scope.modal.isCitiesOpen = true;
    }

    /*
     * Показываем модальное окно с добавлением места
     */
    addNewPlace() {
        this.$scope.modal.isPlaceAddOpen = true;
    }

    /*
     * Выбираем город и фильтруем по нему. Если город пустой - получаем все места
     */
    chooseCity(city) {
        if (city) {
            this.placesService.loadPlaceByCity(city)
                .then(places => {
                    this.$scope.places = places;
                });
        } else {
            this.placesService.loadPlaces()
                .then(places => {
                    this.$scope.places = places;
                });
        }

        // После выбора города, закроем модальное окно
        this.$scope.modal.isCitiesOpen = false;
    }
}

PlacesController.$inject = ['$scope', 'PlacesService'];

export default PlacesController;
